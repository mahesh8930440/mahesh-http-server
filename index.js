const http = require("http");
const fs =require("fs");
const { v4: uuidv4 } = require('uuid');


const port=4000;
const myServer=http.createServer((request,response)=>{
    
    if(request.url ==="/html"){
        response.writeHead(200, { "content-type": "text/html" });
        let htmlContent=fs.readFileSync("data.html","utf-8")
        response.end(htmlContent);
        
    }
    
    else if (request.url ==="/json"){
        response.writeHead(200, { "content-type": "text/json" });
        let jsonData=fs.readFileSync("data.json","utf-8")
        response.end(jsonData);
        
    }

    else if (request.url==="/uuid"){
        response.writeHead(200, { "content-type": "text/application.json" })
        const uniqueId = uuidv4();
        const uuidObject={
            "uuid":uniqueId 
        }
        response.end(JSON.stringify(uuidObject))
       
    }
    
    else if(request.url.startsWith('/status/')){
        const statusCode=parseInt(request.url.split("/")[2]);
        const statusObject = {
            status: statusCode,
            message: http.STATUS_CODES[statusCode],
        };
        response.end(JSON.stringify(statusObject));
    }  

    else if(request.url.startsWith("/delay/")){
        const delayTime=parseInt(request.url.split("/")[2]);
        if (isNaN(delayTime) || delayTime <= 0) {
            response.writeHead(400, { "content-type": "text/application.json" });
            response.end(JSON.stringify({ error: "time delay is failed" }));
            return;
        }
        
        else{
            setTimeout(() =>{
                response.end(JSON.stringify({message:"Success response after delay.",delayTime}))
            },1000*delayTime);
        }
       
    }
        
    
});

myServer.listen(port,()=>{
    console.log("server started");
    
});