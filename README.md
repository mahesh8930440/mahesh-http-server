# HTTP Drill

## Project Setup

- Before starting the implementation, make sure to follow these setup instructions:
  - Create a new repository with the name "Mahesh-Http-server" in your Gitlab subgroup.

## Getting Started

## Steps to setup and run the project

### 1. Clone the repo from GitHub\*\*

- git clone https://gitlab.com/mahesh8930440/mahesh-http-server.git

### 2. Install node and npm.

- sudo apt install nodejs;sudo apt install npm:suso apt install express;


### 3. Navigate to the project directory `http-drill`.

- cd http-drill

### 4. Install npm packages

- This command will install all the dependencies required

  - npm install

### 5. Create Local server

- Go to chrome type localhost:port;